#!/bin/bash
WORKDIR=${0%/*}
OUT=$WORKDIR/roll.pl

echo "Outputting to $OUT" >&2

shopt -s globstar
"$WORKDIR/util/perl-squasher/squash" \
    "$WORKDIR/roll-main.pl" \
    "$WORKDIR"/**/*.pm \
    > "$OUT"
chmod +x -- "$OUT"
