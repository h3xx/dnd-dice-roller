package Dice::Roll;
# :squash-remove-start:
# This file is part of DND Dice Roller.
# Author: Dan Church <h3xx@gmx.com>
# Project URL: https://codeberg.org/h3xx/dnd-dice-roller
# License GPLv3+: GNU GPL version 3 or later (http://gnu.org/licenses/gpl.html).
# :squash-remove-end:
use strict;
use warnings;
use overload
    '""' => 'as_string',
    '0+' => 'total',
    fallback => 1,
    ;

# :squash-remove-start:
require Dice::Roll::Die;
# :squash-remove-end:

sub new {
    my ($class, $throw_type) = @_;

    my @dice;
    foreach my $die_type (split /\+/, $throw_type) {
        my $mult = 1;
        if ($die_type =~ /^(\d+)([Dd].+)$/) {
            $mult = $1;
            $die_type = $2;
        }
        while ($mult-- > 0) {
            push @dice, Dice::Roll::Die->new($die_type);
        }
    }

    return bless {
        dice => \@dice,
    }, $class;
}

sub dice {
    my $self = shift;
    return @{$self->{dice}};
}

sub dice_sorted {
    my $self = shift;
    my @sorted = sort { $a->val <=> $b->val } $self->dice;
    return @sorted;
}

sub discard_high {
    my ($self, $discarding) = @_;
    my @dice = reverse $self->dice_sorted;
    # "Goes to" 0, lol. See https://stackoverflow.com/q/1642028/237955
    while ($discarding --> 0) {
        if (defined (my $die = shift @dice)) {
            $die->discard;
        }
    }
    return;
}

sub discard_low {
    my ($self, $discarding) = @_;
    my @dice = $self->dice_sorted;
    # "Goes to" 0, lol. See https://stackoverflow.com/q/1642028/237955
    while ($discarding --> 0) {
        if (defined (my $die = shift @dice)) {
            $die->discard;
        }
    }
    return;
}

sub total {
    my $self = shift;
    use List::Util qw/ sum /;
    return sum($self->dice);
}

sub as_string {
    my $self = shift;
    return sprintf ' %s  = %d',
        (join '  ', $self->dice),
        $self->total;
}

1;
